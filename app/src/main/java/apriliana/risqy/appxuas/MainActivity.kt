package apriliana.risqy.appxuas

import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit var  db : SQLiteDatabase
    lateinit var fragSetting : SettingActivity
    lateinit var fragSet : SetActivity
    //////////////////////////////////////////////
    //////////////////////////////////////////////
    lateinit var ft : FragmentTransaction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        fragSetting = SettingActivity()
        fragSet = SetActivity()
        ///////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.menu_option,menu)
        return super.onCreateOptionsMenu(menu)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.itemDes ->{
                var intent = Intent(this,SetActivity::class.java)
                startActivity(intent)
            }
            R.id.itemBarang ->{
                var intent = Intent(this,BarangActivity::class.java)
                startActivity(intent)
            }
            R.id.itemJenis ->{
                var intent = Intent(this,JenisActivity::class.java)
                startActivity(intent)
            }
            R.id.itemSuplier -> frameLayout.visibility = View.GONE
        }
        return true
    }
}
