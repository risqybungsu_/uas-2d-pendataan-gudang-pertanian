package apriliana.risqy.appxuas

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_jenis.*
import org.json.JSONArray
import org.json.JSONObject


class JenisActivity : AppCompatActivity(), View.OnClickListener {

    //inisiasi variabel
    lateinit var prodiAdapter : AdapterDataJenis
    var daftarProdi = mutableListOf<HashMap<String,String>>()
    var url4 = "http://192.168.42.207/datagudang/show_data_jenis.php"
    var url5 = "http://192.168.42.207/datagudang/query_upd_del_ins_jenis.php"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_jenis)
        prodiAdapter = AdapterDataJenis(daftarProdi,this)
        listSuplier.layoutManager = LinearLayoutManager(this)
        listSuplier.adapter = prodiAdapter
        btnInsSuplier.setOnClickListener(this)
        btnUpSuplier.setOnClickListener(this)
        btnDelSuplier.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        showDataProdi()
    }

    fun queryInsertUpdateDeleteProdi(mode : String){
        val request = object : StringRequest(
            Method.POST,url5,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi berhasil", Toast.LENGTH_LONG).show()
                    showDataProdi()
                }else{
                    Toast.makeText(this,"Operasi GAGAL", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "insert" ->{
                        hm.put("mode","insert")
                        hm.put("id_jenis",edIdSuplier.text.toString())
                        hm.put("nama_jenis",edNamaSuplier.text.toString())
                    }
                    "update" ->{
                        hm.put("mode","update")
                        hm.put("id_jenis",edIdSuplier.text.toString())
                        hm.put("nama_jenis",edNamaSuplier.text.toString())
                    }
                    "delete" ->{
                        hm.put("mode","delete")
                        hm.put("id_jenis",edIdSuplier.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataProdi(){
        val request = StringRequest(
            Request.Method.POST,url4,
            Response.Listener { response ->
                daftarProdi.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var prodi = HashMap<String,String>()
                    prodi.put("id_jenis",jsonObject.getString("id_jenis"))
                    prodi.put("nama_jenis",jsonObject.getString("nama_jenis"))
                    daftarProdi.add(prodi)
                }
                prodiAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }


    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsSuplier ->{
                queryInsertUpdateDeleteProdi("insert")
            }
            R.id.btnUpSuplier ->{
                queryInsertUpdateDeleteProdi("update")
            }
            R.id.btnDelSuplier ->{
                queryInsertUpdateDeleteProdi("delete")
            }
        }
    }

}