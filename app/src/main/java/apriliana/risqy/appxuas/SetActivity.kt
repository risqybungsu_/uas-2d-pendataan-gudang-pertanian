package apriliana.risqy.appxuas

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import apriliana.risqy.appxuas.R
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.frag_set.*

class SetActivity : AppCompatActivity() {

    //inisiasi variabel
    lateinit var preferences : SharedPreferences
    val PREF_NAME = "setting"
    var BG_TITLE_COLOR = "bg_title_color"
    var FONT_TITLE_COLOR = "font_title_color"
    var FONT_TITLE_SIZE = "font_title__size"
    var TITLE_TEXT = "title_text"
    var DETAIL_FONT_SIZE = "detail_font_size"
    var DETAIL_TEXT = "detail_text"
    val DEF_TITLE_COLOR = "RED"
    val DEF_FONT_TITLE_COLOR  = "WHITE"
    val DEF_FONT_TITLE_SIZE = 24
    val DEF_TITLE_TEXT = "BERAS"
    val DEF_DETAIL_FONT_SIZE = 18
    val DEF_DETAIL_TEXT =
        "Beras adalah bagian bulir padi (gabah) yang telah dipisah dari sekam. Sekam (Jawa merang) secara anatomi disebut 'palea' (bagian yang ditutupi) dan 'lemma' (bagian yang menutupi).Pada salah satu tahap pemrosesan hasil panen padi, gabah ditumbuk dengan lesung atau digiling sehingga bagian luarnya (kulit gabah) terlepas dari isinya. Bagian isi inilah, yang berwarna putih, kemerahan, ungu, atau bahkan hitam, yang disebut beras."
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.frag_set)
    }

    override fun onStart() {
        super.onStart()
        loadSetting()
    }

    fun loadSetting(){
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
//        val DetailLayout = findViewById(R.id.detail) as ConstraintLayout
        txTitle.setBackgroundColor(Color.parseColor(preferences.getString(BG_TITLE_COLOR,DEF_TITLE_COLOR)))
        txTitle.setTextColor(Color.parseColor(preferences.getString(FONT_TITLE_COLOR,DEF_FONT_TITLE_COLOR)))
        txTitle.textSize = preferences.getInt(FONT_TITLE_SIZE,DEF_FONT_TITLE_SIZE).toFloat()
        txDetail.textSize = preferences.getInt(DETAIL_FONT_SIZE,DEF_DETAIL_FONT_SIZE).toFloat()
        txTitle.setText(preferences.getString(TITLE_TEXT,DEF_TITLE_TEXT))
        txDetail.setText(preferences.getString(DETAIL_TEXT,DEF_DETAIL_TEXT))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.menu_option,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemSetting ->{
                var intent = Intent(this,SettingActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }
}